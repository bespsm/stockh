#include <QDir>
#include <QSqlError>
#include "handler_db.h"


namespace model {

HandlerDb::HandlerDb(QObject *parent)
{
    this->setParent(parent);
}

HandlerDb::HandlerDb(QObject *parent, QSqlDatabase *db)
{
    this->setParent(parent);
    db_ = db;
}

void HandlerDb::getRequest(const stock::RequestData* _request)
{

    if (_request->endDate.isNull() ||
        _request->startDate.isNull() ||
        _request->StockSymbol.isNull())
    {
        emit error(stock::HandlerRequestNotCorrect);
        qDebug() << "HandlerRequestNotCorrect";
        return;
    }

    emit processing(_request);


    QFileInfo checkFile(db_->databaseName());
    if (!checkFile.exists() || !checkFile.isFile())
    {
        emit error(stock::DbNoDataBase);
        qDebug() << "FileSistemNoFile " << db_->hostName();
        return;
    }

    if(!db_->open())
    {
        emit error(stock::dbCommonError);
        QSqlError err(db_->lastError());
        qDebug() << "dbCommonError" << err.text();
        qDebug()  <<  QSqlDatabase::drivers();
        return;
    }

    QSqlQuery query;

    QString queryString;
    queryString.append("SELECT Symbol,Date,Open,High,Low,Close,Volume,AdjClose FROM stocks WHERE Symbol = '");
    queryString.append(_request->StockSymbol);
    queryString.append("' AND julianday(Date) >= julianday('");
    queryString.append(_request->startDate.toString(Qt::DateFormat(1)));
    queryString.append("') AND julianday(Date) <= julianday('");
    queryString.append(_request->endDate.toString(Qt::DateFormat(1)));
    queryString.append("') ORDER BY julianday(Date) ASC");

    if(!query.exec(queryString))
    {
        emit error(stock::dbQueryError);
        qDebug() << query.lastError();
        qDebug() << query.executedQuery();
        qDebug() << "dbQueryError";
        return;
    }

    list = new QList<stock::stockData>;
    structureData(query);
    if(list->count() == 0)
    {
        emit error(stock::DbNoDatainDb);
        qDebug() << query.lastError();
        qDebug() << query.executedQuery();
        qDebug() << "DbNoDatainDb";
        return;
    }
    else
    {
        emit done(list);
    }
}

bool HandlerDb::structureData(QSqlQuery query_)
{
    while (query_.next())
    {
        stock::stockData sd;
        sd.StockSymbol = query_.value(0).toString();
        sd.Date = query_.value(1).toDate();
        sd.Open = query_.value(2).toDouble();
        sd.High = query_.value(3).toDouble();
        sd.Low = query_.value(4).toDouble();
        sd.Close = query_.value(5).toDouble();
        sd.Volume = query_.value(6).toDouble();
        sd.AdjClose = query_.value(7).toDouble();
        list->prepend(sd);
    }
    return true;
}

}
