#ifndef HANDLER_DB_H
#define HANDLER_DB_H

#include <model/handler_abstract.h>
#include <QSqlQuery>
#include <QDebug>

class QSqlDatabase;

namespace model {

class HandlerDb : public HandlerAbstract
{
    Q_OBJECT
public:
    explicit HandlerDb(QObject *parent = nullptr);
    HandlerDb(QObject *parent,QSqlDatabase* db);
    ~HandlerDb() { qDebug() << "db deleted"; }
    virtual void getRequest(const stock::RequestData* _request);
signals:
    void done(QList<stock::stockData>*);
    void error(stock::handlersErrors errorCode);
    void processing(const stock::RequestData*);
private:
    bool structureData(QSqlQuery query_);
    QList<stock::stockData>* list;
    QSqlDatabase* db_;
};

}
#endif // HANDLER_DB_H
