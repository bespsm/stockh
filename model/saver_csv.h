#ifndef SAVERCSV_H
#define SAVERCSV_H

#include <QObject>

class saverCsv : public QObject
{
    Q_OBJECT
public:
    explicit saverCsv(QObject *parent = 0);

    QFile *writeToXmlStream(QString symbol);
signals:

public slots:
};

#endif // SAVERCSV_H
