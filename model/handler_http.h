#ifndef HANDLER_HTTP_H
#define HANDLER_HTTP_H

#include <QNetworkAccessManager>
#include <QDebug>
#include <model/handler_abstract.h>

namespace model {

class HandlerHttp : public HandlerAbstract
{
    Q_OBJECT
public:
    explicit HandlerHttp(QObject *parent = nullptr);
    HandlerHttp(QObject *parent, QNetworkAccessManager* _manager);
    ~HandlerHttp() {qDebug() << "bye http";}
    virtual void getRequest(const stock::RequestData * _request);
signals:
    void done(QList<stock::stockData>*);
    void error(stock::handlersErrors errorCode);
    void processing(const stock::RequestData*);
private slots:
     void httpfinished(QNetworkReply* reply);
private:
    QStringList addStockSymbol(QStringList list_);
    bool structureData(const QString _rawData);
    QNetworkAccessManager* manager;
    const stock::RequestData* request;
    QList<stock::stockData>* list;
};
}
#endif // HANDLER_HTTP_H
