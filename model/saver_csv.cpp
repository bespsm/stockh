#include "saverCsv.h"

saverCsv::saverCsv(QObject *parent) : QObject(parent)
{

}

QFile* saverCsv::writeToXmlStream(QString symbol)
{
    QDir xmlDir;
    if (!xmlDir.exists("XmlStockData"))
    {
        xmlDir.current();
        if (!xmlDir.mkdir("XmlStockData"))
        {
            qDebug() << "filysestemerror: don't have access";
            return File;
        }
    }

    QString fileName = symbol + ".xml";

    //fileName.replace(QRegExp("."),"_");

    File = new QFile("XmlStockData/" + fileName);

    if (!File->open(QIODevice::WriteOnly))
    {
        File->close();
        qDebug() << "cant open file";
        return File;
    }

    return File;
}
