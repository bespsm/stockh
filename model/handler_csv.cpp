#include <model/handler_csv.h>
#include <QDir>
#include <QFile>

namespace model {

HandlerCsv::HandlerCsv(QObject *parent)
{
    this->setParent(parent);
}

void HandlerCsv::getRequest(const stock::RequestData *_request)
{
    if (_request->url.isEmpty())
    {
        emit error(stock::HandlerRequestNotCorrect);
        qDebug() << "HandlerRequestNotCorrect";
        return;
    }

    emit processing(_request);

    QFileInfo checkFile(_request->url.path());

    if (!checkFile.exists() || !checkFile.isFile())
    {
        emit error(stock::FileSistemNoFile);
        qDebug() << "FileSistemNoFile " << _request->url.path();
        return;
    }

    QFile file;
    file.setFileName(_request->url.path());

    if (!file.open(QFile::ReadOnly | QFile::Text))
    {
        emit error(stock::FileSistemNoAccesToRead);
        qDebug() << "FileSistemNoAccesToRead";
        return;
    }

    QString data = file.readAll();
    file.close();

    if (data.isEmpty())
    {
        emit error(stock::FileIsEmpty);
        qDebug() << "FileIsEmpty";
        return;
    }

    list = new QList<stock::stockData>;

    if(structureData(data))
    {
        emit done(list);
    }
}

bool HandlerCsv::structureData(const QString _rawData)
{
    //(STOCKSYMBOL,)YYYY-MM-DD,OPEN,HIGH,LOW,CLOSE,VOLUME,ADJCLOSE

    QStringList StringList = _rawData.split(QRegExp("\n|\r\n|\r"),QString::SkipEmptyParts);

    if (StringList.count() < 2)
    {
        emit error(stock::DataIsNotCompatible);
        qDebug() << "DataIsNotСompatible";
        return false;
    }

    for (int i = 1;i < StringList.size();i++)
    {
        QStringList StockList = StringList.at(i).split(",");

        StockList = addStockSymbol(StockList);
        if (StockList.count() != 8)
        {
            emit error(stock::DataIsNotCompatible);
            qDebug() << "DataIsNotСompatible";
            return false;
        }
        stock::stockData sd;
        sd.StockSymbol = StockList.value(0);

        QString rawDate = StockList.value(1);
        QStringList dateList = rawDate.split("-");
        sd.Date = QDate(QString(dateList.value(0)).toInt(),
                        QString(dateList.value(1)).toInt(),
                        QString(dateList.value(2)).toInt());

        sd.Open = QString(StockList.value(2)).toDouble();
        sd.High = QString(StockList.value(3)).toDouble();
        sd.Low = QString(StockList.value(4)).toDouble();
        sd.Close = QString(StockList.value(5)).toDouble();
        sd.Volume = QString(StockList.value(6)).toLong();
        sd.AdjClose = QString(StockList.value(7)).toDouble();

        list->prepend(sd);
    }

    return true;
}


QStringList HandlerCsv::addStockSymbol(QStringList list_)
{
    //doing nothing, actual for http handler
    return list_;
}

}
