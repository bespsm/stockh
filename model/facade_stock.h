#ifndef FACADE_STOCK_H
#define FACADE_STOCK_H

#include <QNetworkAccessManager>
#include <model/handler_abstract.h>
class QSqlDatabase;


namespace controller {
    class RequestStock;
}

namespace model {

#define DATABASE_HOSTNAME       "main"
#define DATABASE_PATH           "db/stocks.db"

class FacadeStock : public QObject
{
    Q_OBJECT
public:
    explicit FacadeStock(QObject *parent = nullptr);
    ~FacadeStock();
    void processRequest(controller::RequestStock &stockReq);

    QStringList& getSymbolsList();
signals:
    void done(QList<stock::stockData>*);
    void error(stock::handlersErrors errorCode);
    void processing(const stock::RequestData*);
    void lastError(stock::handlersErrors errorCode);
    void lastAddedRequest(const stock::RequestData*);
public slots:

private:
    void processByAbstract(HandlerAbstract* virtHandler,controller::RequestStock &stockReq);

    QNetworkAccessManager* nm;
    QSqlDatabase *db;
};

}
#endif // FACADE_STOCK_H
