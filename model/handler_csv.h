#ifndef HANDLER_CSV_H
#define HANDLER_CSV_H

#include "model/handler_abstract.h"
#include <QDebug>

namespace model {


class HandlerCsv : public HandlerAbstract
{
    Q_OBJECT
public:
    explicit HandlerCsv(QObject *parent = nullptr);
    ~HandlerCsv() { qDebug() << "bye bye csv"; }
    virtual void getRequest(const stock::RequestData* _request);
signals:
    void done(QList<stock::stockData>*);
    void error(stock::handlersErrors errorCode);
    void processing(const stock::RequestData*);
public slots:
     QStringList addStockSymbol(QStringList list_);
private:
    bool structureData(const QString _rawData);
    QList<stock::stockData>* list;
};

}
#endif // HANDLER_CSV_H



