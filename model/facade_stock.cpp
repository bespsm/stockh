#include "model/facade_stock.h"
#include "model/handler_csv.h"
#include "model/handler_http.h"
#include "model/handler_db.h"
#include "controller/request_stock.h"
#include "model/handler_xml.h"
#include <QSqlError>
#include <QDir>

namespace model {

FacadeStock::FacadeStock(QObject *parent) : QObject(parent)
{
    this->setParent(parent);

    nm = new QNetworkAccessManager(this);

    db = new QSqlDatabase;
    *db = QSqlDatabase::addDatabase("QSQLITE");
    db->setHostName(DATABASE_HOSTNAME);
    db->setDatabaseName(DATABASE_PATH);
}

FacadeStock::~FacadeStock()
{
    delete nm;
    delete db;
}

void FacadeStock::processRequest(controller::RequestStock &stockReq)
{
    if(stockReq.isInit())
    {
        switch (stockReq.getRequestData()->type)
        {
            case stock::db:
            {
                HandlerDb *handler  = new HandlerDb(this);
                processByAbstract(handler,stockReq);
                break;
            }
            case stock::fileCsv:
            {
                HandlerCsv *handler  = new HandlerCsv(this);
                processByAbstract(handler,stockReq);
                break;
            }
            case stock::fileXml:
            {
                HandlerXml *handler  = new HandlerXml(this);
                processByAbstract(handler,stockReq);
                break;
            }
            case stock::Http:
            {
                HandlerHttp *handler  = new HandlerHttp(this,nm);
                processByAbstract(handler,stockReq);
                break;
            }
        }
    }
    else
    {
        stockReq.addError(stock::HandlerRequestNotCorrect);
        qDebug() << "StockRequest not initialized";
    }
}

void FacadeStock::processByAbstract(HandlerAbstract* virtHandler,controller::RequestStock &stockReq)
{
    connect(virtHandler, SIGNAL(error(stock::handlersErrors)),&stockReq, SLOT(addError(stock::handlersErrors)));
    connect(virtHandler, SIGNAL(done(QList<stock::stockData>*)),&stockReq, SLOT(addHandledData(QList<stock::stockData>*)));
    connect(&stockReq, SIGNAL(handled()),virtHandler, SLOT(deleteLater()));
    virtHandler->getRequest(stockReq.getRequestData());
}


QStringList& FacadeStock::getSymbolsList()
{
    QStringList * list = new QStringList;

    QFileInfo checkFile(db->databaseName());
    if (!checkFile.exists() || !checkFile.isFile())
    {
        qDebug() << "FileSistemNoFile " << db->hostName();
        return *list;
    }

    if(!db->open())
    {
        QSqlError err(db->lastError());
        qDebug() << "dbCommonError" << err.text();
        qDebug()  <<  QSqlDatabase::drivers();
        return *list;
    }

    QSqlQuery query;

    if(!query.exec("SELECT name FROM symbols"))
    {
        qDebug() << query.lastError();
        qDebug() << query.executedQuery();
        qDebug() << "dbQueryError";
        return *list;
    }


    while(query.next())
    {
        list->append(query.value(0).toString());
    }


    if(list->count() == 0)
    {
        qDebug() << query.lastError();
        qDebug() << query.executedQuery();
        qDebug() << "DbNoDatainDb";
        return *list;
    }
    return *list;
}

}
