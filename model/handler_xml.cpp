#include "handler_xml.h"
#include <QXmlStreamWriter>
#include <QFile>
#include <QDir>

namespace model {

HandlerXml::HandlerXml(QObject *parent)
{
    this->setParent(parent);
}

void HandlerXml::getRequest(const stock::RequestData *_request)
{
    if (_request->url.isEmpty())
    {
        emit error(stock::HandlerRequestNotCorrect);
        qDebug() << "HandlerRequestNotCorrect";
        return;
    }

    emit processing(_request);

    QFileInfo checkFile(_request->url.path());

    if (!checkFile.exists() || !checkFile.isFile())
    {
        emit error(stock::FileSistemNoFile);
        qDebug() << "FileSistemNoFile " << _request->url.path();
        return;
    }

    QFile file;
    file.setFileName(_request->url.path());

    if (!file.open(QFile::ReadOnly | QFile::Text))
    {
        emit error(stock::FileSistemNoAccesToRead);
        qDebug() << "FileSistemNoAccesToRead";
        return;
    }

    QString data = file.readAll();
    file.close();

    if (data.isEmpty())
    {
        emit error(stock::FileIsEmpty);
        qDebug() << "FileIsEmpty";
        return;
    }

    list = new QList<stock::stockData>;

    if(structureData(data))
    {
        emit done(list);
    }
}

bool HandlerXml::structureData(const QString _rawData)
{
    QXmlStreamReader xml(_rawData);

    while (!xml.atEnd())
    {
        QXmlStreamReader::TokenType token = xml.readNext();

        if (xml.hasError())
        {
            emit error(stock::xmlCommonError);
            qDebug() << xml.errorString();
            return false;
        }
        if (token == QXmlStreamReader::StartElement && xml.name() == "Stock")
        {
            QXmlStreamAttributes attributes = xml.attributes();
            if (attributes.hasAttribute("Symbol") && attributes.hasAttribute("Date"))
            {
                stock::stockData StockElement;

                StockElement.StockSymbol = attributes.value("Symbol").toString();
                StockElement.Date = QDate::fromString(attributes.value("Date").toString(),Qt::ISODate);

                xml.readNextStartElement();
                while (!(xml.tokenType() == QXmlStreamReader::EndElement && xml.name() == "Stock"))
                {
                    if (xml.tokenType() == QXmlStreamReader::StartElement)
                    {
                        if (xml.name() == "Open"){
                            xml.readNext();
                            StockElement.Open = xml.text().toDouble();}
                        else if (xml.name() == "High"){
                            xml.readNext();
                            StockElement.High = xml.text().toDouble();}
                        else  if (xml.name() == "Low"){
                            xml.readNext();
                            StockElement.Low = xml.text().toDouble();}
                        else  if (xml.name() == "Close"){
                            xml.readNext();
                            StockElement.Close = xml.text().toDouble();}
                        else  if (xml.name() == "Volume"){
                            xml.readNext();
                            StockElement.Volume = xml.text().toInt();}
                        else  if (xml.name() == "AdjClose"){
                            xml.readNext();
                            StockElement.AdjClose = xml.text().toDouble();}
                    }
                    xml.readNextStartElement();
                }
            list->append(StockElement);
            }
        }
    }
    return true;
}

}

/* EXAMPLE
  <Stock Symbol="BAS.DE" Date="2010-01-04">
    <Open>43.46</Open>
    <High>44.85</High>
    <Low>43.35</Low>
    <Close>44.85</Close>
    <Volume>3245600</Volume>
    <AdjClose>36.34</AdjClose>
</Stock>
*/
