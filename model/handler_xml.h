#ifndef HANDLER_XML_H
#define HANDLER_XML_H

#include <QDebug>
#include <model/handler_abstract.h>


namespace model {

class HandlerXml : public HandlerAbstract
{
    Q_OBJECT
public:
    explicit HandlerXml(QObject *parent = nullptr);
    ~HandlerXml() { qDebug() << "bye bye xml"; }
    virtual void getRequest(const stock::RequestData* _request);
signals:
    void done(QList<stock::stockData>*);
    void error(stock::handlersErrors errorCode);
    void processing(const stock::RequestData*);
private:
    bool structureData(const QString _rawData);
    QList<stock::stockData>* list;
};

}
#endif // HANDLER_XML_H
