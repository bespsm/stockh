#ifndef HANDLERABSTRACT_H
#define HANDLERABSTRACT_H

#include <QObject>
#include "types/common_stock.h"

namespace model{

class HandlerAbstract : public QObject
{
    Q_OBJECT
public:
    explicit HandlerAbstract(QObject *parent = nullptr);
    virtual ~HandlerAbstract() {}
public:
   virtual void getRequest(const stock::RequestData* _request) = 0;

signals:
    void done(QList<stock::stockData>*);
    void error(stock::handlersErrors errorCode);
    void processing(const stock::RequestData*);
};

}
#endif // HANDLERABSTRACT_H
