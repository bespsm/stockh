#include <QNetworkReply>
#include "handler_http.h"


namespace model {

HandlerHttp::HandlerHttp(QObject *parent)
{
    this->setParent(parent);

}


HandlerHttp::HandlerHttp(QObject *parent, QNetworkAccessManager* _manager)
{
    this->setParent(parent);
    manager = _manager;
}


void HandlerHttp::getRequest(const stock::RequestData* _request)
{

    if (_request->endDate.isNull() ||
        _request->startDate.isNull() ||
        _request->StockSymbol.isNull())
    {
        emit error(stock::HandlerRequestNotCorrect);
        qDebug() << "HandlerRequestNotCorrect";
        return;
    }

    request = _request;
    emit processing(request);


    if (manager == nullptr)
    {
        emit error(stock::httpManagerNotInitialized);
        qDebug() << "httpManagerNotInitialized";
        return;
    }

    QString host = "http://ichart.yahoo.com/table.csv?";
    QString per = "d";

    QUrl myUrl = QUrl(host +
                      "s=" + request->StockSymbol +
                      "&a=" + QString::number(request->startDate.month() - 1) +
                      "&b=" + QString::number(request->startDate.day()) +
                      "&c=" + QString::number(request->startDate.year()) +
                      "&d=" + QString::number(request->endDate.month() - 1) +
                      "&e=" + QString::number(request->endDate.day()) +
                      "&f=" + QString::number(request->endDate.year()) +
                      "&g=" + per +
                      "&ignore=.csv");

    QNetworkRequest req(myUrl);

     req.setRawHeader("Host","ichart.yahoo.com");
     req.setRawHeader("User-Agent", "Mozilla/5.0");
     req.setRawHeader("Accept-Encoding","identity"); //не шифровать данные
     req.setRawHeader("Connection","keep-alive");

    connect(manager, SIGNAL(finished(QNetworkReply*)),this, SLOT(httpfinished(QNetworkReply*)));

    manager->get(req);
}

void HandlerHttp::httpfinished(QNetworkReply* reply)
{
    if (reply->error() == QNetworkReply::NoError)
    {
        if (reply->bytesAvailable())
        {
            QByteArray respB = reply->readAll();
            QString data = QString::fromUtf8(respB);

            list = new QList<stock::stockData>;

            if(structureData(data))
            {
                emit done(list);
            }
        }
        else
        {
            emit error(stock::httpNoresponse);
            qDebug() << "httpNoresponse";
        }
    }
    else
    {
        emit error(stock::httpCommonError);
        qDebug() << "httpCommonError: " << reply->errorString();
    }
    reply->deleteLater();
}


bool HandlerHttp::structureData(const QString _rawData)
{
    //(STOCKSYMBOL,)YYYY-MM-DD,OPEN,HIGH,LOW,CLOSE,VOLUME,ADJCLOSE

    QStringList StringList = _rawData.split(QRegExp("\n|\r\n|\r"),QString::SkipEmptyParts);

    if (StringList.count() < 2)
    {
        emit error(stock::DataIsNotCompatible);
        qDebug() << "DataIsNotСompatible";
        return false;
    }

    for (int i = 1;i < StringList.size();i++)
    {
        QStringList StockList = StringList.at(i).split(",");

        StockList = addStockSymbol(StockList);
        if (StockList.count() != 8)
        {
            emit error(stock::DataIsNotCompatible);
            qDebug() << "DataIsNotСompatible";
            return false;
        }
        stock::stockData sd;
        sd.StockSymbol = StockList.value(0);

        QString rawDate = StockList.value(1);
        QStringList dateList = rawDate.split("-");
        sd.Date = QDate(QString(dateList.value(0)).toInt(),
                        QString(dateList.value(1)).toInt(),
                        QString(dateList.value(2)).toInt());

        sd.Open = QString(StockList.value(2)).toDouble();
        sd.High = QString(StockList.value(3)).toDouble();
        sd.Low = QString(StockList.value(4)).toDouble();
        sd.Close = QString(StockList.value(5)).toDouble();
        sd.Volume = QString(StockList.value(6)).toLong();
        sd.AdjClose = QString(StockList.value(7)).toDouble();

        list->prepend(sd);
    }

    return true;
}


QStringList HandlerHttp::addStockSymbol(QStringList list_)
{
    list_.prepend(request->StockSymbol);
    return list_;
}

}
