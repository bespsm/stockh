#include "saverXml.h"

saverXml::saverXml(QObject *parent) : QObject(parent)
{

}

void saverXml::writeXml(QList<stockData> structedData,RequestData* reqData)
{
    emit processingRequest(reqData);
    fileHandler = new FileHandler();
    wFile = fileHandler->writeToXmlStream(structedData.at(0).StockSymbol);

    if (!wFile->isOpen())
    {
        emit nextReady();
        return;
    }

    QXmlStreamWriter writer;
    writer.setDevice(wFile);

    writer.setAutoFormatting(true);
    writer.writeStartDocument();
    writer.writeStartElement("HistoryStockData");
    foreach (stockData structure, structedData)
    {
        writer.writeStartElement("Stock");
        writer.writeAttribute("Symbol",structure.StockSymbol);
        writer.writeAttribute("Date",QDate(structure.Date).toString(Qt::DateFormat(1)));
            writer.writeStartElement("Open");
                writer.writeCharacters(QString().setNum(structure.Open));
            writer.writeEndElement();

            writer.writeStartElement("High");
                writer.writeCharacters(QString().setNum(structure.High));
            writer.writeEndElement();

            writer.writeStartElement("Low");
                writer.writeCharacters(QString().setNum(structure.Low));
            writer.writeEndElement();

            writer.writeStartElement("Close");
                writer.writeCharacters(QString().setNum(structure.Close));
            writer.writeEndElement();

            writer.writeStartElement("Volume");
                writer.writeCharacters(QString().setNum(structure.Volume));
            writer.writeEndElement();

            writer.writeStartElement("AdjClose");
                writer.writeCharacters(QString().setNum(structure.AdjClose));
            writer.writeEndElement();
        writer.writeEndElement();
    }
    writer.writeEndElement();
    writer.writeEndDocument();

    wFile->close();
    emit nextReady();
    qDebug() << "Xml save complitely";
}
