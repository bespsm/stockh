#ifndef SAVERXML_H
#define SAVERXML_H

#include <QObject>

class saverXml : public QObject
{
    Q_OBJECT
public:
    explicit saverXml(QObject *parent = 0);

    void writeXml(QList<stockData> structedData, RequestData *reqData);
signals:

public slots:
};

#endif // SAVERXML_H
