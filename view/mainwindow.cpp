#include "view/mainwindow.h"
#include "view/ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setGeometry(400, 250, 542, 390);
    customePlot = new QCustomPlot(this);
    ui->gridLayout->addWidget(customePlot,0,0,1,1);
    setupFinancial();
    customePlot->replot();

    BrowseDialog = new QFileDialog(this);
    BrowseDialog->setFileMode(QFileDialog::ExistingFile);
    BrowseDialog->setOptions(QFileDialog::DontUseNativeDialog);

    //initialize time settings
    ui->dt_from->setMinimumDate(QDate(1990,1,1));
    ui->dt_from->setMaximumDate(QDate::currentDate());
    ui->dt_to->setMinimumDate(QDate(1990,1,1));
    ui->dt_to->setMaximumDate(QDate::currentDate());
    ui->dt_from->setCalendarPopup(true);
    ui->dt_to->setCalendarPopup(true);
    ui->dt_from->setDate(QDate::currentDate().addYears(-1));
    ui->dt_to->setDate(QDate::currentDate());

    //initialize request settings
    ui->tbl_requestHistory->horizontalHeader()->setStretchLastSection(true);
    ui->tbl_requestHistory->horizontalHeader()->setVisible(false);
    ui->tbl_requestHistory->setCornerButtonEnabled(false);
    ui->tbl_requestHistory->setSortingEnabled(true);
    ui->tbl_requestHistory->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->tbl_requestHistory->setModel(nullptr);

    //initialize symbol settings
    ui->tbl_stockSymbols->horizontalHeader()->setStretchLastSection(true);
    ui->tbl_stockSymbols->horizontalHeader()->setVisible(false);
    ui->tbl_stockSymbols->setCornerButtonEnabled(false);
    ui->tbl_stockSymbols->setSortingEnabled(true);
    ui->tbl_stockSymbols->setEditTriggers(QAbstractItemView::NoEditTriggers);

    //init widgets
    ui->dt_from->setEnabled(false);
    ui->dt_to->setEnabled(false);
    ui->tbl_requestHistory->setEnabled(true);
    ui->line_path->setEnabled(false);
    ui->cb_requestType->setEnabled(false);
    ui->btn_apply->setEnabled(false);
    ui->btn_cancel->setEnabled(false);
    ui->btn_browse->setEnabled(false);
    ui->tbl_stockSymbols->setEnabled(false);

    //ui->actionEdit_Request->setCheckable(false);
    reqData.StockSymbol = "";
    reqData.type = (stock::requestType)0;
    reqData.url = "";
    //reqData.startDate = nullptr;
    //reqData.endDate = nullptr;

    //ui state
    windowState = WindowState::initial;

    //emit signalGetSymbols();
}

MainWindow::~MainWindow()
{
    delete ui;
}
  /*
void MainWindow::setupFinancialDemo(QCustomPlot *customPlot)
{

  //demoName = "Financial Charts Demo";
  customPlot->legend->setVisible(true);

  // generate two sets of random walk data (one for candlestick and one for ohlc chart):

  int n = 500;
  QVector<double> time(n), value1(n), value2(n);
  QDateTime start = QDateTime(QDate(2014, 6, 11));
  start.setTimeSpec(Qt::UTC);
  double startTime = start.toTime_t();

  double binSize = 3600*24; // bin data in 1 day intervals

  time[0] = startTime;
  value1[0] = 60;
  value2[0] = 20;
  qsrand(9);
  for (int i=1; i<n; ++i)
  {
    time[i] = startTime + 3600*i;
    value1[i] = value1[i-1] + (qrand()/(double)RAND_MAX-0.5)*10;
    value2[i] = value2[i-1] + (qrand()/(double)RAND_MAX-0.5)*3;
  }


  // create candlestick chart:
  candlesticks = new QCPFinancial(customPlot->xAxis, customPlot->yAxis);
  customPlot->addPlottable(candlesticks);
  //QCPFinancialDataMap data1 = QCPFinancial::timeSeriesToOhlc(time, value1, binSize, startTime);
  candlesticks->setName("Candlestick");
  candlesticks->setChartStyle(QCPFinancial::csCandlestick);
  candlesticks->setData(&data1, true);
  candlesticks->setWidth(binSize*0.5);
  candlesticks->setTwoColored(true);
  candlesticks->setBrushPositive(QColor(245, 0, 0));
  candlesticks->setBrushNegative(QColor(0, 0, 245));
  candlesticks->setPenPositive(QPen(QColor(0, 0, 0)));
  candlesticks->setPenNegative(QPen(QColor(0, 0, 0)));

  // create ohlc chart:
  ohlc = new QCPFinancial(customPlot->xAxis, customPlot->yAxis);
  customPlot->addPlottable(ohlc);
  QCPFinancialDataMap data2 = QCPFinancial::timeSeriesToOhlc(time, value2, binSize/3.0, startTime); // divide binSize by 3 just to make the ohlc bars a bit denser
  ohlc->setName("OHLC");
  ohlc->setChartStyle(QCPFinancial::csOhlc);
  ohlc->setData(&data2, true);
  ohlc->setWidth(binSize*0.1);
  ohlc->setTwoColored(true);

  // create bottom axis rect for volume bar chart:
  volumeAxisRect = new QCPAxisRect(customPlot);
  customPlot->plotLayout()->addElement(1, 0, volumeAxisRect);
  volumeAxisRect->setMaximumSize(QSize(QWIDGETSIZE_MAX, 100));
  volumeAxisRect->axis(QCPAxis::atBottom)->setLayer("axes");
  volumeAxisRect->axis(QCPAxis::atBottom)->grid()->setLayer("grid");
  // bring bottom and main axis rect closer together:
  customPlot->plotLayout()->setRowSpacing(0);
  volumeAxisRect->setAutoMargins(QCP::msLeft|QCP::msRight|QCP::msBottom);
  volumeAxisRect->setMargins(QMargins(0, 0, 0, 0));
  // create two bar plottables, for positive (green) and negative (red) volume bars:
  volumePos = new QCPBars(volumeAxisRect->axis(QCPAxis::atBottom), volumeAxisRect->axis(QCPAxis::atLeft));
  volumeNeg = new QCPBars(volumeAxisRect->axis(QCPAxis::atBottom), volumeAxisRect->axis(QCPAxis::atLeft));
  for (int i=0; i<n/5; ++i)
  {
    int v = qrand()%20000+qrand()%20000+qrand()%20000-10000*3;
    (v < 0 ? volumeNeg : volumePos)->addData(startTime+3600*5.0*i, qAbs(v)); // add data to either volumeNeg or volumePos, depending on sign of v
  }
  customPlot->setAutoAddPlottableToLegend(false);
  customPlot->addPlottable(volumePos);
  customPlot->addPlottable(volumeNeg);
  volumePos->setWidth(3600*4);
  volumePos->setPen(Qt::NoPen);
  volumePos->setBrush(QColor(100, 180, 110));
  volumeNeg->setWidth(3600*4);
  volumeNeg->setPen(Qt::NoPen);
  volumeNeg->setBrush(QColor(180, 90, 90));

  // interconnect x axis ranges of main and bottom axis rects:
  connect(customPlot->xAxis, SIGNAL(rangeChanged(QCPRange)), volumeAxisRect->axis(QCPAxis::atBottom), SLOT(setRange(QCPRange)));
  connect(volumeAxisRect->axis(QCPAxis::atBottom), SIGNAL(rangeChanged(QCPRange)), customPlot->xAxis, SLOT(setRange(QCPRange)));
  // configure axes of both main and bottom axis rect:
  volumeAxisRect->axis(QCPAxis::atBottom)->setAutoTickStep(false);
  volumeAxisRect->axis(QCPAxis::atBottom)->setTickStep(3600*24*4); // 4 day tickstep
  volumeAxisRect->axis(QCPAxis::atBottom)->setTickLabelType(QCPAxis::ltDateTime);
  volumeAxisRect->axis(QCPAxis::atBottom)->setDateTimeSpec(Qt::UTC);
  volumeAxisRect->axis(QCPAxis::atBottom)->setDateTimeFormat("dd. MMM");
  volumeAxisRect->axis(QCPAxis::atBottom)->setTickLabelRotation(15);
  volumeAxisRect->axis(QCPAxis::atLeft)->setAutoTickCount(3);
  customPlot->xAxis->setBasePen(Qt::NoPen);
  customPlot->xAxis->setTickLabels(false);
  customPlot->xAxis->setTicks(false); // only want vertical grid in main axis rect, so hide xAxis backbone, ticks, and labels
  customPlot->xAxis->setAutoTickStep(false);
  customPlot->xAxis->setTickStep(3600*24*4); // 4 day tickstep
  customPlot->rescaleAxes();
  customPlot->xAxis->scaleRange(1.025, customPlot->xAxis->range().center());
  customPlot->yAxis->scaleRange(1.1, customPlot->yAxis->range().center());

  // make axis rects' left side line up:
  QCPMarginGroup *group = new QCPMarginGroup(customPlot);
  customPlot->axisRect()->setMarginGroup(QCP::msLeft|QCP::msRight, group);
  volumeAxisRect->setMarginGroup(QCP::msLeft|QCP::msRight, group);

}
*/

void MainWindow::setupFinancial()
{
    customePlot->legend->setVisible(true);
    customePlot->setInteraction(QCP::iRangeZoom,true);   // Включаем взаимодействие удаления/приближения
    customePlot->setInteraction(QCP::iRangeDrag, true);  // Включаем взаимодействие перетаскивания графика
    customePlot->axisRect()->setRangeDrag(Qt::Horizontal);   // Включаем перетаскивание только по горизонтальной оси
    customePlot->axisRect()->setRangeZoom(Qt::Horizontal);   // Включаем удаление/приближение только по горизонтальной оси

    customePlot->xAxis2->setVisible(true);
    customePlot->yAxis2->setVisible(true);
    customePlot->xAxis2->setTicks(false);
    customePlot->yAxis2->setTicks(false);
    customePlot->xAxis2->setTickLabels(false);
    customePlot->yAxis2->setTickLabels(false);

    //verticalLine = new QCPCurve(customePlot->xAxis, customePlot->yAxis);
    //customePlot->addPlottable(verticalLine);
    //verticalLine->setName("Vertical");

    graph = new QCPGraph(customePlot->xAxis, customePlot->yAxis);
    customePlot->addPlottable(graph);
    graph->setName("graph");

    tracer = new QCPItemTracer(customePlot);
    tracer->setInterpolating(false);
    tracer->setStyle( QCPItemTracer::tsCrosshair);


    candlesticks = new QCPFinancial(customePlot->xAxis, customePlot->yAxis);
    customePlot->addPlottable(candlesticks);
    candlesticks->setName("Candlestick");
    candlesticks->setChartStyle(QCPFinancial::csCandlestick);
    //candlesticks->setChartStyle(QCPFinancial::csOhlc);
    candlesticks->setTwoColored(true);
    candlesticks->setBrushPositive(QColor(245, 0, 0));
    candlesticks->setBrushNegative(QColor(0, 0, 245));
    candlesticks->setPenPositive(QPen(QColor(0, 0, 0)));
    candlesticks->setPenNegative(QPen(QColor(0, 0, 0)));
    customePlot->setVisible(false);

    customePlot->axisRect()->setVisible(true);

    customePlot->xAxis->setAutoTickStep(true);
    //customePlot->xAxis->setAutoTickStep(false);
    //customePlot->xAxis->setTickStep(3600*24*4);
    customePlot->xAxis->setTickLabelType(QCPAxis::ltDateTime);
    customePlot->xAxis->setDateTimeSpec(Qt::UTC);
    customePlot->xAxis->setDateTimeFormat("dd.MM.yyyy");
    customePlot->xAxis->setLabel("Date");
    customePlot->yAxis->setLabel("Stock");


    connect(customePlot->xAxis,SIGNAL(rangeChanged(QCPRange)),this,SLOT(slotRangeChanged(QCPRange)));
    connect(customePlot, &QCustomPlot::mousePress, this, &MainWindow::slotMousePress);
}


void MainWindow::plotStockData(const QList<stock::stockData> *data)
{
    customePlot->setVisible(true);
    QCPFinancialDataMap* dataMap = new QCPFinancialDataMap;
    QCPDataMap * graphData = new QCPDataMap;
    foreach(auto dataPint, *data)
    {
        double date = QDateTime(dataPint.Date).toTime_t();
        QCPFinancialData point = QCPFinancialData(date,dataPint.Open,dataPint.High,dataPint.Low,dataPint.Close);
        dataMap->insert(date,point);

        QCPData graphPoint = QCPData(date,dataPint.Open);
        graphData->insert(date,graphPoint);
        /*qDebug() << dataPint.StockSymbol
                 << " " << QDate(dataPint.Date).toString(Qt::DateFormat(1))
                 << " " << dataPint.Open
                 << " " << dataPint.High
                 << " " << dataPint.Low
                 << " " << dataPint.Close
                 << " " << dataPint.Volume
                 << " " << dataPint.AdjClose;*/
    }


    double binSize = 3600*24; // bin data in 1 day intervals
    candlesticks->clearData();
    candlesticks->setData(dataMap,false);
    candlesticks->setWidth(binSize);
    candlesticks->setVisible(true);

    maxPlotRange.lower = dataMap->firstKey() - 3600*24;
    maxPlotRange.upper = dataMap->lastKey() + 3600*24;

    graph->clearData();
    graph->setData(graphData,false);
    graph->setAntialiased(false);   //сглживание
    graph->setVisible(false);
    tracer->setGraph(graph);
    //customePlot->xAxis->setRange(maxPlotRange);
    //customePlot->xAxis->scaleRange(1.02, customePlot->xAxis->range().center());
    //customePlot->yAxis->scaleRange(1.1, customePlot->yAxis->range().center());
    customePlot->rescaleAxes();

    //maxPlotRange.lower = customePlot->xAxis->range().lower;
   // maxPlotRange.upper = customePlot->xAxis->range().upper;


    //connect(customePlot->xAxis,SIGNAL(rangeChanged(QCPRange)),this,SLOT(slotRangeChanged(QCPRange)));
    customePlot->replot();
}

void MainWindow::slotMousePress(QMouseEvent *event)
{
    double rawX = customePlot->xAxis->pixelToCoord(event->pos().x());
    tracer->setGraphKey(rawX);

    double key = tracer->position->key();
    uint date = key;
    QCPFinancialData point = candlesticks->data()->value(key);

    QString mess =
            QString("day: %1, open: %2, close: %3, low: %4, high: %5")
            .arg(QDateTime::fromTime_t(date).toString("dd.MM.yyyy"))
            .arg(QString::number(point.open,'f',2))
            .arg(QString::number(point.close,'f',2))
            .arg(QString::number(point.low,'f',2))
            .arg(QString::number(point.high,'f',2));


    ui->statusBar->showMessage(mess,0);
    customePlot->replot();
    //verticalLine->setData(dataMap,false);
    //verticalLine->setData(x, y);
}

void MainWindow::slotRangeChanged(QCPRange newRange)
{
   if (newRange.lower < maxPlotRange.lower) {
       customePlot->xAxis->setRangeLower(maxPlotRange.lower);
   }
   if (newRange.upper > maxPlotRange.upper) {
       customePlot->xAxis->setRangeUpper(maxPlotRange.upper);
   }
   if ((newRange.upper - newRange.lower) < 3600*24)
   {
       customePlot->xAxis->setRange(newRange.lower,newRange.lower + 3600*24);
   }
}
/*
void MainWindow::slotMouseMove(QMouseEvent *event)
{
    if(QApplication::mouseButtons()) slotMousePress(event);
}*/



void MainWindow::updateStockSymbols(QStringList& symbolsList)
{
    symbolModel = &symbolsList;
    symbolModel->append("none");
    QItemSelectionModel *m = ui->tbl_stockSymbols->selectionModel();
    QStringListModel *newModel = new QStringListModel(symbolsList,this);
    ui->tbl_stockSymbols->setModel(newModel);
    delete m;
}

void MainWindow::udpateRequestHistory(QStringList &requestList)
{
    int curRow = ui->tbl_requestHistory->selectionModel()->currentIndex().row();
    delete ui->tbl_requestHistory->selectionModel();

    requestModel = &requestList;
    QStringListModel *newModel = new QStringListModel(requestList,this);
    ui->tbl_requestHistory->setModel(newModel);
    windowState_prev == newRequest ?
                ui->tbl_requestHistory->selectRow((requestList.count() - 1)) :
                ui->tbl_requestHistory->selectRow(curRow);

    this->on_tbl_requestHistory_clicked(ui->tbl_requestHistory->selectionModel()->currentIndex());
}


void MainWindow::getStock(const controller::RequestStock& stock)
{
    //switch (stock.getTypeRequest())
    switch (stock.getRequestData()->type)
    {
    case stock::db:
        ui->cb_requestType->setCurrentIndex(1);
        ui->dt_from->setDate(stock.getRequestData()->startDate);
        ui->dt_to->setDate(stock.getRequestData()->endDate);
        ui->line_path->setText("");
        break;
   case stock::Http:
        ui->cb_requestType->setCurrentIndex(2);
        ui->dt_from->setDate(stock.getRequestData()->startDate);
        ui->dt_to->setDate(stock.getRequestData()->endDate);
        ui->line_path->setText("");
        break;
    default:
        ui->cb_requestType->setCurrentIndex(0);
        ui->dt_from->setDate(QDate::currentDate().addYears(-1));
        ui->dt_to->setDate(QDate::currentDate());
        ui->line_path->setText(stock.getRequestData()->url.fileName());
        break;
    }
    int symbolIndex = symbolModel->indexOf(stock.getRequestData()->StockSymbol);
    symbolIndex == -1 ? ui->tbl_stockSymbols->selectRow(symbolModel->count() - 1) : ui->tbl_stockSymbols->selectRow(symbolIndex);
    stock.isError() == true ? customePlot->setVisible(false) : plotStockData(stock.getStockData());
    ui->statusBar->showMessage(stock.getErrorText(),2000);
}


void MainWindow::setCtlPanelStateInitial()
{
    if (this->windowState ==  WindowState::newRequest ||this->windowState == WindowState::editRequest)
    {
        this->windowState_prev = this->windowState;
        //ui->line_path->setText("");
        ui->dt_from->setEnabled(false);
        ui->dt_to->setEnabled(false);

        ui->tbl_requestHistory->setEnabled(true);

        ui->line_path->setEnabled(false);
        ui->cb_requestType->setEnabled(false);

        ui->btn_apply->setEnabled(false);
        ui->btn_cancel->setEnabled(false);
        ui->btn_browse->setEnabled(false);

        ui->tbl_stockSymbols->setEnabled(false);

        this->windowState = WindowState::initial;
    }
}

void MainWindow::setCtlPanelStateTypeFile()
{
    if (this->windowState ==  WindowState::initial)
    {
        ui->tbl_requestHistory->setEnabled(false);
        ui->btn_apply->setEnabled(true);
        ui->btn_cancel->setEnabled(true);
        ui->cb_requestType->setEnabled(true);
        ui->line_path->setText("");
    }
    ui->btn_browse->setEnabled(true);
    ui->dt_from->setEnabled(false);
    ui->dt_to->setEnabled(false);
    ui->tbl_stockSymbols->setEnabled(false);
}

void MainWindow::setCtlPanelStateTypeExt()
{
    if (this->windowState ==  WindowState::initial)
    {
        ui->tbl_requestHistory->setEnabled(false);
        ui->btn_apply->setEnabled(true);
        ui->btn_cancel->setEnabled(true);
        ui->cb_requestType->setEnabled(true);
        ui->line_path->setText("");
    }
    ui->btn_browse->setEnabled(false);
    ui->dt_from->setEnabled(true);
    ui->dt_to->setEnabled(true);
    ui->tbl_stockSymbols->setEnabled(true);
}


void MainWindow::on_tbl_stockSymbols_clicked(const QModelIndex &index)
{
    reqData.StockSymbol = index.data().toString();
}

void MainWindow::on_tbl_requestHistory_clicked(const QModelIndex &index)
{
    reqNum = index.row();
    emit signalGetStock(reqNum);
}


void MainWindow::on_actionRequest_Del_triggered()
{
    if (this->windowState == WindowState::initial)
    {
        if (requestModel->count() == 1)
        {
            requestModel->clear();
            QStringListModel *newModel = new QStringListModel(*requestModel,this);
            ui->tbl_requestHistory->setModel(newModel);
            customePlot->setVisible(false);
            //customePlot->replot();
            emit siginalDeleteRequest(0);
        }
        else emit siginalDeleteRequest(reqNum);
    }
}


void MainWindow::on_cb_requestType_currentIndexChanged(const QString &arg1)
{
    if (windowState == WindowState::newRequest || windowState ==  WindowState::editRequest)
    {
        this->on_actionNew_Request_triggered();
    }
}


void MainWindow::on_actionNew_Request_triggered()
{
    if (ui->cb_requestType->currentText() == "Local File")
    {
        this->setCtlPanelStateTypeFile();
    }
    else if(ui->cb_requestType->currentText() == "Local DB")
    {
        this->setCtlPanelStateTypeExt();
        reqData.type = stock::db;
        //reqData.url = "";
    }
    else if(ui->cb_requestType->currentText() == "YAHHOO")
    {
        this->setCtlPanelStateTypeExt();
        reqData.type = stock::Http;
        //reqData.url = "";
    }
    this->windowState =  WindowState::newRequest;
}

void MainWindow::on_actionEdit_Request_triggered()
{
    if (ui->cb_requestType->currentText() == "Local File")
    {
        this->setCtlPanelStateTypeFile();
    }
    else if(ui->cb_requestType->currentText() == "Local DB")
    {
        this->setCtlPanelStateTypeExt();
        reqData.type = stock::db;
        //reqData.url = "";
    }
    else if(ui->cb_requestType->currentText() == "YAHHOO")
    {
        this->setCtlPanelStateTypeExt();
        reqData.type = stock::Http;
        //reqData.url = "";
    }
    this->windowState =  WindowState::editRequest;
}


void MainWindow::on_btn_browse_clicked()
{
    reqData.url = BrowseDialog->getOpenFileUrl(
      this,
      tr("Open local file"),
      tr("/home"),
      tr("XML File (*.xml);;CSV files (*.csv)"));

    if (reqData.url.fileName().contains(".xml",Qt::CaseInsensitive))
    {
        ui->line_path->setText(reqData.url.fileName());
        reqData.type = stock::fileXml;
    }
    else if (reqData.url.fileName().contains(".csv",Qt::CaseInsensitive))
    {
        ui->line_path->setText(reqData.url.fileName());
        reqData.type = stock::fileCsv;
    }
    else reqData.type = (stock::requestType)0;
}

void MainWindow::on_btn_cancel_clicked()
{
    this->setCtlPanelStateInitial();
}

void MainWindow::on_btn_apply_clicked()
{
    if (stock::isCorrectRequest(&reqData))
    {
        stock::RequestData* newReqData = new stock::RequestData;
        newReqData->endDate = reqData.endDate;
        newReqData->startDate = reqData.startDate;
        newReqData->StockSymbol = reqData.StockSymbol;
        newReqData->type = reqData.type;
        newReqData->url = reqData.url;

        if (windowState == MainWindow::editRequest)
           emit signalEditRequest(reqNum,newReqData);
        else if (windowState == MainWindow::newRequest)
            emit signalNewRequest(newReqData);

        this->setCtlPanelStateInitial();
    }
    else
    {
        QMessageBox mBox;
        mBox.setText("request not correct");
        mBox.setInformativeText("Choose correct parameters");
        mBox.setIcon(QMessageBox::Information);
        mBox.exec();
    }
}

void MainWindow::on_dt_from_userDateChanged(const QDate &date)
{
    reqData.startDate = date;
}

void MainWindow::on_dt_to_userDateChanged(const QDate &date)
{
    reqData.endDate = date;
}

void MainWindow::on_actionClose_control_panel_triggered()
{
    ui->dockWidget->close();
}

void MainWindow::on_actionShow_control_panel_triggered()
{
    ui->dockWidget->show();
}
