#ifndef MAINWINDOW_H
#define MAINWINDOW_H

//#include <QTimer>
//#include <QList>
//#include <QDesktopWidget>
//#include <QMessageBox>
//#include <QMetaEnum>
#include <libs/customeplot/qcustomplot.h>
//#include "types/common_stock.h"
#include "controller/request_stock.h"



namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    enum WindowState{
        initial,
        newRequest,
        editRequest};

    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
     void setupFinancial();

signals:
     void signalNewRequest(stock::RequestData * reqData);
     void signalEditRequest(const int num, stock::RequestData * reqData);
     void siginalDeleteRequest(const int num);
     void signalGetStock(const int num);

public slots:
     void updateStockSymbols(QStringList& symbolsList);
     void udpateRequestHistory(QStringList& requestList);
     void getStock(const controller::RequestStock& stock);

private slots:
     void setCtlPanelStateInitial();
     void setCtlPanelStateTypeFile();
     void setCtlPanelStateTypeExt();
     void plotStockData(const QList<stock::stockData>* data);

     void on_actionRequest_Del_triggered();
     void on_actionNew_Request_triggered();
     void on_btn_browse_clicked();
     void on_cb_requestType_currentIndexChanged(const QString &arg1);
     void on_btn_cancel_clicked();
     void on_btn_apply_clicked();
     void on_actionClose_control_panel_triggered();
     void on_actionShow_control_panel_triggered();
     void on_dt_from_userDateChanged(const QDate &date);
     void on_dt_to_userDateChanged(const QDate &date);
     void on_tbl_stockSymbols_clicked(const QModelIndex &index);
     void on_actionEdit_Request_triggered();
     void on_tbl_requestHistory_clicked(const QModelIndex &index);

     void slotMousePress(QMouseEvent * event);

     void slotRangeChanged(QCPRange newRange);
private:
    Ui::MainWindow *ui;
    QCustomPlot* customePlot;
    QCPFinancial *candlesticks;
    QCPFinancial *ohlc;
    QCPGraph *graph;
    QCPRange maxPlotRange;
    //QCPAxisRect *volumeAxisRect;
    //QCPBars *volumePos;
    //QCPBars *volumeNeg;
    //QCPCurve *verticalLine;
    QCPItemTracer *tracer;

    MainWindow::WindowState windowState;
    MainWindow::WindowState windowState_prev = WindowState::newRequest;

    QFileDialog* BrowseDialog;
    stock::RequestData reqData;
    int reqNum;
    QStringList* symbolModel;
    QStringList* requestModel;
};

#endif // MAINWINDOW_H
