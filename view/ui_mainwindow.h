/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QDockWidget>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTableView>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionRequest_Del;
    QAction *actionEdit_Request;
    QAction *actionNew_Request;
    QAction *actionClose_control_panel;
    QAction *actionShow_control_panel;
    QWidget *centralWidget;
    QGridLayout *gridLayout_3;
    QGridLayout *gridLayout;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuView;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;
    QDockWidget *dockWidget;
    QWidget *dockWidgetContents;
    QVBoxLayout *verticalLayout_5;
    QVBoxLayout *verticalLayout;
    QLabel *label_5;
    QComboBox *cb_requestType;
    QVBoxLayout *verticalLayout_4;
    QLabel *label_6;
    QHBoxLayout *horizontalLayout;
    QLineEdit *line_path;
    QToolButton *btn_browse;
    QGridLayout *gridLayout_2;
    QLabel *label_2;
    QLabel *label;
    QDateEdit *dt_from;
    QDateEdit *dt_to;
    QVBoxLayout *verticalLayout_3;
    QLabel *label_4;
    QTableView *tbl_stockSymbols;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *btn_apply;
    QPushButton *btn_cancel;
    QVBoxLayout *verticalLayout_2;
    QFrame *line;
    QLabel *label_3;
    QTableView *tbl_requestHistory;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(780, 704);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Minimum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        MainWindow->setMinimumSize(QSize(780, 704));
        MainWindow->setBaseSize(QSize(800, 600));
        MainWindow->setDocumentMode(false);
        MainWindow->setDockOptions(QMainWindow::AllowTabbedDocks|QMainWindow::AnimatedDocks|QMainWindow::VerticalTabs);
        actionRequest_Del = new QAction(MainWindow);
        actionRequest_Del->setObjectName(QStringLiteral("actionRequest_Del"));
        QIcon icon;
        icon.addFile(QStringLiteral(":/icons/gnome-desktop-icons-png/PNG/32/Gnome-Edit-Delete-32.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionRequest_Del->setIcon(icon);
        actionEdit_Request = new QAction(MainWindow);
        actionEdit_Request->setObjectName(QStringLiteral("actionEdit_Request"));
        QIcon icon1;
        icon1.addFile(QStringLiteral(":/icons/gnome-desktop-icons-png/PNG/32/Gnome-Accessories-Text-Editor-32.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionEdit_Request->setIcon(icon1);
        actionNew_Request = new QAction(MainWindow);
        actionNew_Request->setObjectName(QStringLiteral("actionNew_Request"));
        QIcon icon2;
        icon2.addFile(QStringLiteral(":/icons/gnome-desktop-icons-png/PNG/32/Gnome-Document-New-32.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionNew_Request->setIcon(icon2);
        actionClose_control_panel = new QAction(MainWindow);
        actionClose_control_panel->setObjectName(QStringLiteral("actionClose_control_panel"));
        actionShow_control_panel = new QAction(MainWindow);
        actionShow_control_panel->setObjectName(QStringLiteral("actionShow_control_panel"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout_3 = new QGridLayout(centralWidget);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        gridLayout = new QGridLayout();
        gridLayout->setSpacing(0);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));

        gridLayout_3->addLayout(gridLayout, 0, 0, 1, 1);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 780, 25));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        menuView = new QMenu(menuBar);
        menuView->setObjectName(QStringLiteral("menuView"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        mainToolBar->setMaximumSize(QSize(1677215, 16777215));
        mainToolBar->setMovable(false);
        mainToolBar->setAllowedAreas(Qt::NoToolBarArea);
        mainToolBar->setIconSize(QSize(24, 24));
        mainToolBar->setFloatable(false);
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        statusBar->setSizeGripEnabled(true);
        MainWindow->setStatusBar(statusBar);
        dockWidget = new QDockWidget(MainWindow);
        dockWidget->setObjectName(QStringLiteral("dockWidget"));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(dockWidget->sizePolicy().hasHeightForWidth());
        dockWidget->setSizePolicy(sizePolicy1);
        dockWidget->setMinimumSize(QSize(201, 615));
        dockWidget->setMaximumSize(QSize(201, 2048));
        dockWidget->setAutoFillBackground(false);
        dockWidget->setFloating(false);
        dockWidget->setFeatures(QDockWidget::AllDockWidgetFeatures);
        dockWidget->setAllowedAreas(Qt::LeftDockWidgetArea|Qt::RightDockWidgetArea);
        dockWidgetContents = new QWidget();
        dockWidgetContents->setObjectName(QStringLiteral("dockWidgetContents"));
        verticalLayout_5 = new QVBoxLayout(dockWidgetContents);
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setContentsMargins(11, 11, 11, 11);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setSizeConstraint(QLayout::SetNoConstraint);
        label_5 = new QLabel(dockWidgetContents);
        label_5->setObjectName(QStringLiteral("label_5"));
        sizePolicy1.setHeightForWidth(label_5->sizePolicy().hasHeightForWidth());
        label_5->setSizePolicy(sizePolicy1);

        verticalLayout->addWidget(label_5);

        cb_requestType = new QComboBox(dockWidgetContents);
        cb_requestType->setObjectName(QStringLiteral("cb_requestType"));
        cb_requestType->setEnabled(true);
        sizePolicy1.setHeightForWidth(cb_requestType->sizePolicy().hasHeightForWidth());
        cb_requestType->setSizePolicy(sizePolicy1);

        verticalLayout->addWidget(cb_requestType);


        verticalLayout_5->addLayout(verticalLayout);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        verticalLayout_4->setSizeConstraint(QLayout::SetNoConstraint);
        label_6 = new QLabel(dockWidgetContents);
        label_6->setObjectName(QStringLiteral("label_6"));
        sizePolicy1.setHeightForWidth(label_6->sizePolicy().hasHeightForWidth());
        label_6->setSizePolicy(sizePolicy1);

        verticalLayout_4->addWidget(label_6);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setSizeConstraint(QLayout::SetFixedSize);
        line_path = new QLineEdit(dockWidgetContents);
        line_path->setObjectName(QStringLiteral("line_path"));
        sizePolicy1.setHeightForWidth(line_path->sizePolicy().hasHeightForWidth());
        line_path->setSizePolicy(sizePolicy1);

        horizontalLayout->addWidget(line_path);

        btn_browse = new QToolButton(dockWidgetContents);
        btn_browse->setObjectName(QStringLiteral("btn_browse"));

        horizontalLayout->addWidget(btn_browse);


        verticalLayout_4->addLayout(horizontalLayout);


        verticalLayout_5->addLayout(verticalLayout_4);

        gridLayout_2 = new QGridLayout();
        gridLayout_2->setSpacing(6);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        gridLayout_2->setSizeConstraint(QLayout::SetNoConstraint);
        label_2 = new QLabel(dockWidgetContents);
        label_2->setObjectName(QStringLiteral("label_2"));
        sizePolicy1.setHeightForWidth(label_2->sizePolicy().hasHeightForWidth());
        label_2->setSizePolicy(sizePolicy1);

        gridLayout_2->addWidget(label_2, 0, 0, 1, 1);

        label = new QLabel(dockWidgetContents);
        label->setObjectName(QStringLiteral("label"));
        sizePolicy1.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy1);

        gridLayout_2->addWidget(label, 0, 1, 1, 1);

        dt_from = new QDateEdit(dockWidgetContents);
        dt_from->setObjectName(QStringLiteral("dt_from"));
        QSizePolicy sizePolicy2(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(dt_from->sizePolicy().hasHeightForWidth());
        dt_from->setSizePolicy(sizePolicy2);

        gridLayout_2->addWidget(dt_from, 1, 0, 1, 1);

        dt_to = new QDateEdit(dockWidgetContents);
        dt_to->setObjectName(QStringLiteral("dt_to"));
        sizePolicy2.setHeightForWidth(dt_to->sizePolicy().hasHeightForWidth());
        dt_to->setSizePolicy(sizePolicy2);

        gridLayout_2->addWidget(dt_to, 1, 1, 1, 1);


        verticalLayout_5->addLayout(gridLayout_2);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        label_4 = new QLabel(dockWidgetContents);
        label_4->setObjectName(QStringLiteral("label_4"));
        sizePolicy1.setHeightForWidth(label_4->sizePolicy().hasHeightForWidth());
        label_4->setSizePolicy(sizePolicy1);

        verticalLayout_3->addWidget(label_4);

        tbl_stockSymbols = new QTableView(dockWidgetContents);
        tbl_stockSymbols->setObjectName(QStringLiteral("tbl_stockSymbols"));
        sizePolicy1.setHeightForWidth(tbl_stockSymbols->sizePolicy().hasHeightForWidth());
        tbl_stockSymbols->setSizePolicy(sizePolicy1);
        tbl_stockSymbols->setMaximumSize(QSize(181, 141));
        tbl_stockSymbols->setEditTriggers(QAbstractItemView::CurrentChanged);

        verticalLayout_3->addWidget(tbl_stockSymbols);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setSizeConstraint(QLayout::SetNoConstraint);
        btn_apply = new QPushButton(dockWidgetContents);
        btn_apply->setObjectName(QStringLiteral("btn_apply"));
        sizePolicy1.setHeightForWidth(btn_apply->sizePolicy().hasHeightForWidth());
        btn_apply->setSizePolicy(sizePolicy1);

        horizontalLayout_2->addWidget(btn_apply);

        btn_cancel = new QPushButton(dockWidgetContents);
        btn_cancel->setObjectName(QStringLiteral("btn_cancel"));
        sizePolicy1.setHeightForWidth(btn_cancel->sizePolicy().hasHeightForWidth());
        btn_cancel->setSizePolicy(sizePolicy1);

        horizontalLayout_2->addWidget(btn_cancel);


        verticalLayout_3->addLayout(horizontalLayout_2);


        verticalLayout_5->addLayout(verticalLayout_3);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        line = new QFrame(dockWidgetContents);
        line->setObjectName(QStringLiteral("line"));
        sizePolicy2.setHeightForWidth(line->sizePolicy().hasHeightForWidth());
        line->setSizePolicy(sizePolicy2);
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        verticalLayout_2->addWidget(line);

        label_3 = new QLabel(dockWidgetContents);
        label_3->setObjectName(QStringLiteral("label_3"));
        sizePolicy1.setHeightForWidth(label_3->sizePolicy().hasHeightForWidth());
        label_3->setSizePolicy(sizePolicy1);

        verticalLayout_2->addWidget(label_3);

        tbl_requestHistory = new QTableView(dockWidgetContents);
        tbl_requestHistory->setObjectName(QStringLiteral("tbl_requestHistory"));
        QSizePolicy sizePolicy3(QSizePolicy::Fixed, QSizePolicy::Expanding);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(tbl_requestHistory->sizePolicy().hasHeightForWidth());
        tbl_requestHistory->setSizePolicy(sizePolicy3);
        tbl_requestHistory->setMinimumSize(QSize(181, 141));
        tbl_requestHistory->setMaximumSize(QSize(181, 1000));
        tbl_requestHistory->setSizeIncrement(QSize(181, 141));
        tbl_requestHistory->setBaseSize(QSize(181, 141));

        verticalLayout_2->addWidget(tbl_requestHistory);


        verticalLayout_5->addLayout(verticalLayout_2);

        dockWidget->setWidget(dockWidgetContents);
        MainWindow->addDockWidget(static_cast<Qt::DockWidgetArea>(1), dockWidget);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuView->menuAction());
        menuFile->addAction(actionNew_Request);
        menuView->addAction(actionClose_control_panel);
        menuView->addAction(actionShow_control_panel);
        menuView->addSeparator();
        mainToolBar->addAction(actionNew_Request);
        mainToolBar->addAction(actionEdit_Request);
        mainToolBar->addAction(actionRequest_Del);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        actionRequest_Del->setText(QApplication::translate("MainWindow", "Request Del", 0));
        actionEdit_Request->setText(QApplication::translate("MainWindow", "actionEditRequest", 0));
        actionNew_Request->setText(QApplication::translate("MainWindow", "New Request", 0));
        actionClose_control_panel->setText(QApplication::translate("MainWindow", "close control panel", 0));
        actionShow_control_panel->setText(QApplication::translate("MainWindow", "show control panel", 0));
        menuFile->setTitle(QApplication::translate("MainWindow", "File", 0));
        menuView->setTitle(QApplication::translate("MainWindow", "view", 0));
        dockWidget->setWindowTitle(QApplication::translate("MainWindow", "tools", "comment disq"));
        label_5->setText(QApplication::translate("MainWindow", "source:", 0));
        cb_requestType->clear();
        cb_requestType->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "Local File", 0)
         << QApplication::translate("MainWindow", "Local DB", 0)
         << QApplication::translate("MainWindow", "YAHHOO", 0)
        );
        label_6->setText(QApplication::translate("MainWindow", "path:", 0));
        btn_browse->setText(QApplication::translate("MainWindow", "...", 0));
        label_2->setText(QApplication::translate("MainWindow", "from:", 0));
        label->setText(QApplication::translate("MainWindow", "to:", 0));
        label_4->setText(QApplication::translate("MainWindow", "stock symbols:", 0));
        btn_apply->setText(QApplication::translate("MainWindow", "apply", 0));
        btn_cancel->setText(QApplication::translate("MainWindow", "cancel", 0));
        label_3->setText(QApplication::translate("MainWindow", "request history:", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
