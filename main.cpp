#include "controller/controller_stock.h"
#include "view/mainwindow.h"
#include <QApplication>



int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    MainWindow maniW;
    controller::ControllerStock ctl;


    /*QObject::connect(&maniW,SIGNAL(signalGetSymbols()),
                     &ctl,SLOT(slotUpdateSymbols()));*/

    /*QObject::connect(&ctl,SIGNAL(signalGetSymbols(const QStringList&)),
                     &maniW,SLOT(updateStockSymbols(const QStringList&)));*/

    QObject::connect(&ctl,SIGNAL(signalUpdateRequests(QStringList&)),
                     &maniW,SLOT(udpateRequestHistory(QStringList&)));

    QObject::connect(&maniW,SIGNAL(signalNewRequest(stock::RequestData*)),
                     &ctl,SLOT(newRequest(stock::RequestData*)));

    QObject::connect(&maniW,SIGNAL(signalEditRequest(const int, stock::RequestData*)),
                     &ctl,SLOT(editRequest(const int, stock::RequestData *)));

    QObject::connect(&maniW,SIGNAL(siginalDeleteRequest(const int)),
                     &ctl,SLOT(deleteRequestInList(const int)));

    QObject::connect(&maniW,SIGNAL(signalGetStock(const int)),
                     &ctl,SLOT(getStock(const int)));

    QObject::connect(&ctl,SIGNAL(signalGetStock(const controller::RequestStock&)),
                     &maniW,SLOT(getStock(const controller::RequestStock&)));

    maniW.updateStockSymbols(ctl.slotUpdateSymbols());

    maniW.show();

    return a.exec();
}


