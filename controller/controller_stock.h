#ifndef CONTROLLERSTOCK_H
#define CONTROLLERSTOCK_H

#include <controller/request_stock.h>

class FacadeStock;

namespace controller {

class ControllerStock : public QObject
{
    Q_OBJECT
public:
    ~ControllerStock() {}
    explicit ControllerStock(QObject *parent = nullptr);
    QStringList& getRequests() {return *listRequestsTime;}

public slots:
    void getStock(const int num);
    void deleteRequestInList(const int num);
    void newRequest(stock::RequestData* reqData);
    void editRequest(const int num, stock::RequestData * reqData);
    QStringList& slotUpdateSymbols();
    void slotUpdateRequests();
signals:
    void signalUpdateRequests(QStringList& list);
    void signalGetSymbols(const QStringList& list);
    void signalGetStock(const controller::RequestStock& stock);


private:
    model::FacadeStock* modelStock;
    QList<RequestStock*> listRequests;
    QStringList* listRequestsTime;
};

}

#endif // CONTROLLERSTOCK_H
