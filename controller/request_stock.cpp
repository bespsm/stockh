#include "request_stock.h"

namespace controller {

RequestStock::RequestStock(QObject *parent) : QObject(parent)
{
    this->setParent(parent);
    //timer.setInterval(100000);
    //timer.start();
}

RequestStock::~RequestStock()
{
    delete _requestData;
    _stockData->clear();
}

void RequestStock::init(stock::RequestData *requestData)
{
    //this->initPtr();
    _requestData = requestData;
    lastError = stock::noError;
    lastErrorText = "no Error";
}


bool RequestStock::isInit()
{
    if (_requestData == nullptr)
    {
        return false;
    }
     return true;
}

bool RequestStock::isComplete()
{
   if  (_stockData == nullptr) return false;
   else return true;
}
/*
bool RequestStock::isError() const
{
    if  (lastError == stock::noError) return false;
    else return true;
}

QString StockRequest::getLastChangedTime()
{
    QString str;
    return str.setNum(timer.interval());
}*/

void RequestStock::addHandledData(QList<stock::stockData>* stockData)
{
    _stockData = stockData;
    //timer.stop();
    lastChangedTime = QDateTime::currentDateTime();
    emit handled();
}

void RequestStock::addError(stock::handlersErrors err)
{
    lastError = err;
    switch (err)
    {
    case stock::FileSistemNoFile:
        lastErrorText = "File System: No File";
        break;
    case stock::FileSistemNoAccesToRead:
        lastErrorText = "File Sistem: No Acces To Read";
        break;
    case stock::FileIsEmpty:
        lastErrorText = "File System: File Is Empty";
        break;
    case stock::FileIsNotCompatible:
        lastErrorText = "File System: File Is Not Compatible";
        break;
    case stock::DataIsNotCompatible:
        lastErrorText = "File System: Data Is Not Compatible";
        break;
    case stock::DbNoDataBase:
        lastErrorText = "Db: No Data Base";
        break;
    case stock::DbNoDatainDb:
        lastErrorText = "Db: No Data in Db";
        break;
    case stock::HandlerRequestNotCorrect:
        lastErrorText = "Handler: Request Not Correct";
        break;
    case stock::httpManagerNotInitialized:
        lastErrorText = "http: Manager Not Initialized";
        break;
    case stock::httpNoresponse:
        lastErrorText = "http: No response";
        break;
    case stock::httpCommonError:
        lastErrorText = "http: Common Error";
        break;
    case stock::xmlCommonError:
        lastErrorText = "xml: Common Error";
        break;
    case stock::dbCommonError:
        lastErrorText = "db: Common Error";
        break;
    case stock::dbQueryError:
        lastErrorText = "db: Query Error";
        break;
    case stock::noError:
        lastErrorText = "no Error";
        break;
    }

    //timer.stop();
    lastChangedTime = QDateTime::currentDateTime();
    emit handled();
}

void RequestStock::initPtr()
{
    delete _requestData;
    _stockData->clear();
    lastError = stock::noError;
}

}
