#ifndef STOCKREQUEST_H
#define STOCKREQUEST_H


#include <types/common_stock.h>
#include <model/facade_stock.h>


namespace model {
    class FacadeStock;
}

namespace controller {

class RequestStock : public QObject
{
    Q_OBJECT
public:
    friend class model::FacadeStock;

    explicit RequestStock(QObject *parent = nullptr);
    ~RequestStock();
    void init(stock::RequestData* requestData);
    bool isInit();
    bool isComplete();
    bool  isError() const {return lastError == stock::noError ? false : true; }

    QString getErrorText() const { return lastErrorText; }
    QList<stock::stockData>* getStockData() const { return _stockData; }
    stock::RequestData* getRequestData()  const { return _requestData; }
    QString getLastChangedTime() { return lastChangedTime.toString("hh:mm:ss");}
    stock::handlersErrors getError() { return lastError;}
signals:
    void handled();

private slots:
    void addHandledData(QList<stock::stockData>* stockData);   //for friends )
    void addError(stock::handlersErrors err);   //for friends )

private:
    void initPtr();
    stock::RequestData* _requestData;
    QList<stock::stockData>* _stockData;
    stock::requestType _requestType;
    stock::handlersErrors lastError;
    QString lastErrorText;
    QDateTime lastChangedTime;
};

}

#endif // STOCKREQUEST_H
