#include "controller_stock.h"

namespace controller {

ControllerStock::ControllerStock(QObject *parent) : QObject(parent)
{
    modelStock = new model::FacadeStock(this);
    listRequestsTime = new QStringList;
}

void ControllerStock::getStock(const int num)
{
    if (listRequests.count() >= num)
    {
        emit signalGetStock(*listRequests.at(num));
    }
}

QStringList& ControllerStock::slotUpdateSymbols()
{
    QStringList& sList = modelStock->getSymbolsList();
    emit signalGetSymbols(sList);
    return sList;
}

void ControllerStock::slotUpdateRequests()
{
    listRequestsTime->clear();
    if (listRequests.count() != 0)
    {
        foreach (auto Iter, listRequests) {

            listRequestsTime->append(Iter->getLastChangedTime());
        }
     emit signalUpdateRequests(*listRequestsTime);
    }
}

void ControllerStock::newRequest(stock::RequestData *reqData)
{
    RequestStock* req = new RequestStock(this);
    req->init(reqData);
    connect(req, SIGNAL(handled()),this, SLOT(slotUpdateRequests()));
    listRequests.append(req);
    modelStock->processRequest(*req);
}

void ControllerStock::editRequest(const int num, stock::RequestData *reqData)
{
    if (listRequests.count() >= num)
    {
        RequestStock* req = listRequests.at(num);
        req->init(reqData);
        connect(req, SIGNAL(handled()),this, SLOT(slotUpdateRequests()));
        modelStock->processRequest(*req);
    }
}

void ControllerStock::deleteRequestInList(const int num)
{
    listRequests.count() == 1 ? listRequests.clear() : listRequests.removeAt(num);
    this->slotUpdateRequests();
}

}
