#include "types/common_stock.h"

namespace stock {



bool isCorrectRequest(stock::RequestData* req)
{
    if (req->type != 0)
    {
        if (req->type == fileXml || req->type == fileCsv)
        {
            if (!req->url.isEmpty())
            {
             return true;
            }
        }
        else if (req->type == db || req->type == Http)
        {
            if (!(req->startDate.isNull()) && !(req->endDate.isNull()) && !(req->StockSymbol.isEmpty()))
            {
                if (req->startDate.year() == req->endDate.year())
                {
                    if(req->startDate.month() == req->endDate.month())
                    {
                        if(req->startDate.day() < req->endDate.day())
                            return true;
                    }
                    else if(req->startDate.month() < req->endDate.month())
                        return true;
                }
                else if (req->startDate.year() < req->endDate.year())
                    return true;
            }
        }
    }
    return false;
}

}
