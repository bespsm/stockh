
#ifndef COMMON_STOCK_H
#define COMMON_STOCK_H

#include <QDate>
#include <QUrl>

namespace stock {

enum requestType{
    fileXml = 1,
    fileCsv,
    Http,
    db
};

enum handlersErrors{
    FileSistemNoFile,
    FileSistemNoAccesToRead,
    FileIsEmpty,
    FileIsNotCompatible,
    DataIsNotCompatible,
    DbNoDataBase,
    DbNoDatainDb,
    HandlerRequestNotCorrect,
    httpManagerNotInitialized,
    httpNoresponse,
    httpCommonError,
    xmlCommonError,
    dbCommonError,
    dbQueryError,
    noError
};

struct RequestData
{
    stock::requestType type;
    QUrl url;
    QString StockSymbol;
    QDate startDate;
    QDate endDate;
};


struct stockData
{
    QString StockSymbol;
    QDate Date;
    double Open;
    double High;
    double Low;
    double Close;
    int Volume;
    double AdjClose;
};

bool isCorrectRequest(stock::RequestData* req);

}
#endif // COMMON_STOCK_H
