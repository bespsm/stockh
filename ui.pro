#-------------------------------------------------
#
# Project created by QtCreator 2016-02-17T03:07:26
#
#-------------------------------------------------

QT       += core gui network sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = ui
TEMPLATE = app
CONFIG += c++11

SOURCES += main.cpp\
    view/mainwindow.cpp \
    libs/customeplot/qcustomplot.cpp \
    controller/controller_stock.cpp \
    controller/request_stock.cpp \
    model/facade_stock.cpp \
    model/handler_abstract.cpp \
    model/handler_csv.cpp \
    model/handler_db.cpp \
    model/handler_http.cpp \
    model/handler_xml.cpp \
    types/common_stock.cpp


HEADERS  += \
    view/mainwindow.h \
    libs/customeplot/qcustomplot.h \
    model/handler_csv.h \
    model/handler_http.h \
    model/handler_xml.h \
    model/handler_db.h \
    controller/controller_stock.h \
    controller/request_stock.h \
    model/facade_stock.h \
    view/ui_mainwindow.h \
    model/handler_abstract.h \
    types/common_stock.h

FORMS    += view/mainwindow.ui

RESOURCES += sources/res.qrc

copydata.commands = $(COPY_DIR) $$PWD/db $$OUT_PWD
first.depends = $(first) copydata
export(first.depends)
export(copydata.commands)
QMAKE_EXTRA_TARGETS += first copydata
